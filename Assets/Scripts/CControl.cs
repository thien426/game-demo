﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class CControl : MonoBehaviour
{
    public static CControl instance = null;
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    float DELTA_X_MAX;
    Vector2 movePos;

    Vector2 downPos;
    float downTime;
    float upTime = -1f;

    CMain main;

    public void Init()
    {
        DELTA_X_MAX = Screen.width / 11;

        main = CGameStage.instance.main;
    }

    public void PointDown(BaseEventData baseEvent)
    {
        PointerEventData e = baseEvent as PointerEventData;

        downPos = e.position;

        downTime = Time.realtimeSinceStartup;

        float deltaTime = downTime - upTime;

        if (deltaTime < 0.3f && !CGameStage.instance.isPlay) CGameStage.instance.PlayGame();
    }

    public void Move(BaseEventData baseEvent)
    {
        PointerEventData e = baseEvent as PointerEventData;

        movePos = e.position;

        float deltaX = movePos.x - downPos.x;

        if (!CGameStage.instance.isPlay) main.MoveProcessBall(deltaX / DELTA_X_MAX);
        else main.MoveProcessX(deltaX / DELTA_X_MAX);

        downPos = movePos;
    }

    public void PointUp(BaseEventData baseEvent)
    {
        PointerEventData e = baseEvent as PointerEventData;

        Vector2 upPos = e.position;

        upTime = downTime;
    }
}

