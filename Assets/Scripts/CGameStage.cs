﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGameStage : MonoBehaviour
{
    public static CGameStage instance = null;
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    public CMain main;

    public CWallManager wallManager;

    public bool isPlay = false;
    public int totalRound;

    public int totalWall;
    public int totalBall;

    private void Start()
    {
        CUserInfo.Instance.level--;

        CControl.instance.Init();

        InstanceGame();
    }

    public void InstanceGame()
    {
        CUserInfo.Instance.level++;
        CUserInfo.Instance.Save();

        totalRound = 2;

        totalBall = 0;

        totalWall = 0;

        isPlay = false;

        main.Init();

        wallManager.Init();

        CPopupManager.instance.SetTextCountBallIngame(totalRound);
    }

    public void PlayGame()
    {
        isPlay = true;

        main.PlayGame();
    }

    public void SetTotalBall()
    {
        if (totalBall == 0)
        {
            isPlay = false;

            main.InitBallMain();

            totalRound--;

            if (totalRound > -1) CPopupManager.instance.SetTextCountBallIngame(totalRound);
            else CPopupManager.instance.ShowResult();
        }
    }

    public void RemoveAllChildren(Transform trans)
    {
        for (int i = trans.childCount - 1; i >= 0; i--)
        {
            Destroy(trans.GetChild(i).gameObject);
        }

        trans.DetachChildren();
    }
}
