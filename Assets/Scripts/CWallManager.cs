﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CWallManager : MonoBehaviour
{
    public static CWallManager instance = null;
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    [SerializeField] CWallRow[] listRow;

    #region BEFORE_PLAY
    public void Init()
    {
        for (int i = 1; i < listRow.Length; i++) listRow[i].Init();

        InstanceGame();
    }
    #endregion

    #region INSTANCE_GAME
    public CDataLevel level;

    void InstanceGame()
    {
        level = CDataManager.Instance.Levels.GetLevelByID(CUserInfo.Instance.LevelData);
        int totalRow = level.GetTotalItems();
        
        for (int i = 1; i < totalRow + 1; i++)
        {
            listRow[i].indexRow = i;
            listRow[i].InstanceGame();
        }
        
        CBarManager.instance.SetPositionBar(listRow[totalRow].transform.position);
    }
    #endregion
}
