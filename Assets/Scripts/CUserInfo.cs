﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
public class CUserInfo
{
    public const string SAVED_FN = "userInfo.dat";
    string persistentDataPath;

    [SerializeField] public int level = 1;
    public int LevelData
    {
        get
        {
            int totalLevel = CDataManager.Instance.Levels.LevelLength();
            int resetToLevel = 1;
            int delta = totalLevel - resetToLevel + 1;

            if (level > totalLevel)
                return (level - totalLevel - 1) % delta + resetToLevel;
            else return level;
        }
    }

    static CUserInfo _instance;
    public static CUserInfo Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new CUserInfo();

                string fullPath = _instance.persistentDataPath + "/" + SAVED_FN;
                if (File.Exists(fullPath))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream file = File.Open(fullPath, FileMode.Open);
                    _instance = (CUserInfo)bf.Deserialize(file);
                    file.Close();
                }

                _instance.InitAfterLoaded();
            }
            return _instance;
        }
    }

    public CUserInfo() { Init(); }

    void Init()
    {
        persistentDataPath = Application.persistentDataPath;
    }

    void InitAfterLoaded()
    {
        
    }

    public void Save(bool needSync = false)
    {
        string fullPath = persistentDataPath + "/" + SAVED_FN;
        if (GetType().IsSerializable)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(fullPath, FileMode.OpenOrCreate);
            bf.Serialize(file, this);
            file.Flush();
            file.Close();
        }
    }
}