﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CStar : MonoBehaviour
{
    [SerializeField] bool isIncreaseProcess;
    [SerializeField] bool isAddProcess;
    [SerializeField] bool isAddBall;

    public void SetRigid()
    {
        Rigidbody2D rigid = GetComponentInChildren<Rigidbody2D>();

        rigid.gravityScale = 1f;
    }

    public void OnChildrenTriggerEnter2D(CColliderParams param)
    {
        if (param.other.layer == LayerMask.NameToLayer("Process"))
        {
            if (param.other.GetComponentInParent<CProcess>().isMain)
            {
                if (isIncreaseProcess)
                {
                    CGameStage.instance.main.IncreaseProcess();
                }
                else if (isAddProcess)
                {
                    CGameStage.instance.main.processAdd.AddProcess();
                }
                else
                {
                    CGameStage.instance.main.ballManager.AddBall();
                }
            }

            Destroy(gameObject);
        }
    }

    public void OnChildrenTriggerStay2D(CColliderParams param)
    {

    }

    public void OnChildrenTriggerExit2D(CColliderParams param)
    {

    }
}
