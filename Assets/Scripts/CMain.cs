﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMain : MonoBehaviour
{
    public CProcess process;
    public CProcess processAdd;
    public CBallManager ballManager;
    public CBall ballMain;

    float maxXBall;
    float maxXProcess;

    #region BEFORE_PLAY
    private void Start()
    {
        maxXBall = 3.7f;
    }

    public void Init()
    {
        maxXProcess = maxXBall;

        process.isMain = true;

        process.Init();

        processAdd.Init();

        ballManager.Init();

        ballMain = ballManager.GetBallMain();
    }

    public void MoveProcessBall(float xMove)
    {
        MoveProcessX(xMove);

        MoveBallX(xMove);
    }

    public void MoveProcessX(float xMove)
    {
        Vector2 posProcess = process.transform.position;

        float posX = posProcess.x + xMove;

        if (posX > 0) posProcess.x = Mathf.Min(maxXProcess, posX);
        else posProcess.x = Mathf.Max(-maxXProcess, posX);

        process.transform.position = posProcess;
    }

    void MoveBallX(float xMove)
    {
        Vector2 posBall = ballMain.transform.position;

        float posX = posBall.x + xMove;

        if (posX > 0) posBall.x = Mathf.Min(maxXBall, posX);
        else posBall.x = Mathf.Max(-maxXBall, posX);

        ballMain.transform.position = posBall;
    }
    #endregion

    #region PLAY_GAME
    public void PlayGame()
    {
        ballMain.Play();
    }

    public void IncreaseProcess()
    {
        process.IncreaseScale();

        maxXProcess -= 0.3f;
    }

    public void InitBallMain()
    {
        ballManager.InstanceBallMain();

        ballMain = ballManager.GetBallMain();
    }
    #endregion
}
