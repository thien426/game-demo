﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPopupIngame : MonoBehaviour
{
    #region LEVEL_BUTTON
    public void Level_1()
    {
        LevelButton(1);
    }

    public void Level_2()
    {
        LevelButton(2);
    }

    public void Level_3()
    {
        LevelButton(3);
    }

    public void Level_4()
    {
        LevelButton(4);
    }

    public void Level_5()
    {
        LevelButton(5);
    }

    public void Level_6()
    {
        LevelButton(6);
    }

    public void Level_7()
    {
        LevelButton(7);
    }

    public void Level_8()
    {
        LevelButton(8);
    }

    public void Level_9()
    {
        LevelButton(9);
    }

    public void Level_10()
    {
        LevelButton(10);
    }

    public void Level_11()
    {
        LevelButton(11);
    }

    public void Level_12()
    {
        LevelButton(12);
    }

    public void Level_13()
    {
        LevelButton(13);
    }

    public void Level_14()
    {
        LevelButton(14);
    }

    public void Level_15()
    {
        LevelButton(15);
    }

    public void Level_16()
    {
        LevelButton(16);
    }

    public void Level_17()
    {
        LevelButton(17);
    }

    public void Level_18()
    {
        LevelButton(18);
    }
    #endregion

    void LevelButton(int level)
    {
        CUserInfo.Instance.level = level - 1;

        CGameStage.instance.InstanceGame();
    }

    [SerializeField] Text countRound;

    public void SetTextCountBall(int value)
    {
        countRound.text = "" + value;
    }
}
