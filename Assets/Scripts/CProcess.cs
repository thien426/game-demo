﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CProcess : MonoBehaviour
{
    Vector2 posProcessStart;

    public bool isMain = false;

    List<GameObject> listProcessAdd;

    void Start()
    {
        posProcessStart = transform.position;
    }

    public void Init()
    {
        CGameStage.instance.RemoveAllChildren(transform);

        transform.position = posProcessStart;

        posXOld = transform.position.x;

        if (isMain)
        {
            GameObject prefab = Resources.Load<GameObject>("Prefab/Process/Process_1");

            Instantiate(prefab, transform);
        }

        listProcessAdd = new List<GameObject>();

        ResetScale();
    }

    #region SCALE_PROCESS
    public void IncreaseScale()
    {
        Vector2 scale = transform.localScale;

        scale.x += 0.5f;

        transform.localScale = scale;
    }

    void ResetScale()
    {
        Vector2 scale = transform.localScale;

        scale.x = 1f;

        transform.localScale = scale;
    }
    #endregion

    #region ADD_PROCESS
    public void AddProcess()
    {
        GameObject prefab = Resources.Load<GameObject>("Prefab/Process/Process_2");

        GameObject processAdd = Instantiate(prefab, transform);

        listProcessAdd.Add(processAdd);

        Move();
    }

    void Move()
    {
        float speed = 70f;

        Rigidbody2D rigid = listProcessAdd[listProcessAdd.Count - 1].GetComponentInChildren<Rigidbody2D>();
        rigid.AddForce(Vector2.right * speed);
    }
    #endregion

    float posXOld;
    float speedNow;
    void Update()
    {
        if (CGameStage.instance.isPlay)
        {
            float posXNow = transform.position.x;

            speedNow = (posXNow - posXOld) / Time.deltaTime;

            posXOld = posXNow;
        }
        // tinh vantoc
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Rigidbody2D rigid = collision.gameObject.GetComponent<Rigidbody2D>();
        rigid.AddForce(Vector2.right * speedNow * 200f);
    }
}
