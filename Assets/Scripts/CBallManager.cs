﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBallManager : MonoBehaviour
{
    [SerializeField] GameObject parentBallMain;
    [SerializeField] GameObject parentBallAdd;

    CBall ballMain;
    List<CBall> listBallAdd;

    #region Init
    public void Init()
    {
        CGameStage.instance.RemoveAllChildren(parentBallAdd.transform);

        CGameStage.instance.RemoveAllChildren(parentBallMain.transform);

        InstanceBallMain();

        listBallAdd = new List<CBall>();
    }

    public void InstanceBallMain()
    {
        ballMain = InstanceBall(parentBallMain.transform);

        ballMain.Init();

        CGameStage.instance.totalBall++;
    }

    public CBall GetBallMain()
    {
        return ballMain;
    }
    #endregion

    #region ADD_BALL
    public void AddBall()
    {
        CBall ball = InstanceBall(parentBallAdd.transform);

        ball.Play();

        listBallAdd.Add(ball);

        CGameStage.instance.totalBall++;
    }

    CBall InstanceBall(Transform transParent)
    {
        CBall prefab = Resources.Load<CBall>("Prefab/Balls/Ball_1");

        CBall ball = Instantiate(prefab, transParent);

        Vector2 posBall = ball.transform.position;

        posBall.x = CGameStage.instance.main.process.transform.position.x;

        ball.transform.position = posBall;

        return ball;
    }
    #endregion
}
