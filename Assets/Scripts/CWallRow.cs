﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CWallRow : MonoBehaviour
{
    public int indexRow;
    public CWallBase[] listWall;
    public CBar[] listBar;

    float distanceWall = 0.65f;
    float posXStart = -2.925f;

    #region Init
    public void Init()
    {
        CGameStage.instance.RemoveAllChildren(transform);
    }
    #endregion

    #region INSTANCE_GAME
    public void InstanceGame()
    {
        InstanceListBar();

        InstanceListWall();

        InstanceListStar();
    }
    #endregion

    #region INSTANCE_WALL
    void InstanceListWall()
    {
        List<int> listWallData = CWallManager.instance.level.GetItem(indexRow).listWallInRow;
        List<int> listPositionWallData = CWallManager.instance.level.GetItem(indexRow).listPositionWall;
        
        listWall = new CWallBase[listWallData.Count];

        CGameStage.instance.totalWall += listWallData.Count;

        for (int i = 0; i < listWallData.Count; i++)
        {
            CWallBase wall = InstanceWall(listWallData[i]);

            Vector2 posWall = wall.transform.position;
            posWall.x = posXStart + distanceWall * listPositionWallData[i];
            wall.transform.position = posWall;

            listWall[i] = wall;
        }
    }

    CWallBase InstanceWall(int indexWall)
    {
        string path = "Prefab/Walls/Wall_" + indexWall;

        CWallBase prefabWall = Resources.Load<CWallBase>(path);

        CWallBase wall = Instantiate(prefabWall, transform);

        return wall;
    }
    #endregion

    #region INSTANCE_BAR
    void InstanceListBar()
    {
        List<int> listPositionBarData = CWallManager.instance.level.GetItem(indexRow).listPositionBar;

        if (listPositionBarData.Count > 0)
        {
            listBar = new CBar[listPositionBarData.Count];

            for (int i = 0; i < listPositionBarData.Count; i++)
            {
                CBar bar = InstanceBar();

                Vector2 posBar = bar.transform.position;
                posBar.x = posXStart + distanceWall * listPositionBarData[i];
                bar.transform.position = posBar;

                listBar[i] = bar;
            }
        }
    }

    CBar InstanceBar()
    {
        CBar prefabBar = Resources.Load<CBar>("Prefab/Bar/Bar");

        CBar bar = Instantiate(prefabBar, transform);

        return bar;
    }
    #endregion

    #region INSTANCE_STAR
    List<int> listIndexWallConstainStar;

    void InstanceListStar()
    {
        int countStarProcess = CWallManager.instance.level.GetItem(indexRow).countStarProcessInRow;
        int countStarBall = CWallManager.instance.level.GetItem(indexRow).countStarBallInRow;

        int totalStar = countStarProcess + countStarBall;
        int totalWall = listWall.Length;

        if (totalStar > 0 && totalStar <= totalWall)
        {
            SetListIndexWallConstainStar(totalStar, totalWall);

            if (countStarProcess > 0) InstanceProcessStar(countStarProcess);

            if (countStarBall > 0) InstanceBallStar(countStarBall);
        }
    }

    void SetListIndexWallConstainStar(int totalStar, int totalWall)
    {
        listIndexWallConstainStar = new List<int>();

        for (int i = 0; i < totalWall; i++) listIndexWallConstainStar.Add(i);

        int totalIndexRemove = totalWall - totalStar;

        for (int i = 0; i < totalIndexRemove; i++) listIndexWallConstainStar.RemoveAt(Random.Range(0, listIndexWallConstainStar.Count));
    }

    void InstanceProcessStar(int cout)
    {
        int indexStarProcessInRow = CWallManager.instance.level.GetItem(indexRow).indexStarProcessInRow;

        InstanceStar(indexStarProcessInRow, cout);
    }

    void InstanceBallStar(int cout)
    {
        int indexStarBallInRow = CWallManager.instance.level.GetItem(indexRow).indexStarBallInRow;

        InstanceStar(indexStarBallInRow, cout);
    }

    void InstanceStar(int indexStar, int totalStar)
    {
        for (int i = 0; i < totalStar; i++)
        {
            int index = Random.Range(0, listIndexWallConstainStar.Count);
            int indexWall = listIndexWallConstainStar[index];

            listIndexWallConstainStar.RemoveAt(index);

            listWall[indexWall].InstanceStar(indexStar);
        }
    }
    #endregion
}
