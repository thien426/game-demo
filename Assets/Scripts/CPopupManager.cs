﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPopupManager : MonoBehaviour
{
    public static CPopupManager instance = null;
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    #region RESULT
    [SerializeField] CPopupResult popupResult;

    public void ShowResult()
    {
        popupResult.Show();
    }
    #endregion

    #region INGAME
    [SerializeField] CPopupIngame popupIngame;

    public void SetTextCountBallIngame(int value)
    {
        popupIngame.SetTextCountBall(value);
    }
    #endregion
}
