﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBall : MonoBehaviour
{
    Vector2 vectorForce;
    float speed;

    public void Init()
    {
        Vector2 posBall = CGameStage.instance.main.process.transform.localPosition;

        posBall.y = 0;

        transform.localPosition = posBall;
    }

    public void Play()
    {
        speed = Mathf.Min(110f + 10f * CUserInfo.Instance.level, 200f);

        vectorForce = Vector2.up * 2f + Vector2.right;

        Rigidbody2D rigid = GetComponent<Rigidbody2D>();
        rigid.AddForce(vectorForce * speed);
    }

    public void OnChildrenTriggerEnter2D(CColliderParams param)
    {
        if (param.other.layer == LayerMask.NameToLayer("Wall"))
        {
            param.other.GetComponentInParent<CWallBase>().HideWall();
        }
        else if(param.other.layer == LayerMask.NameToLayer("Destroy Ball"))
        {
            CGameStage.instance.totalBall--;

            CGameStage.instance.SetTotalBall();

            Destroy(this.gameObject);
        }
    }

    public void OnChildrenTriggerStay2D(CColliderParams param)
    {

    }

    public void OnChildrenTriggerExit2D(CColliderParams param)
    {

    }
}
