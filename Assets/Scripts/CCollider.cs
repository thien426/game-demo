﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CCollider : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        CColliderParams param = new CColliderParams();
        param.owner = gameObject;
        param.other = collision.gameObject;
        SendMessageUpwards("OnChildrenTriggerEnter2D", param, SendMessageOptions.RequireReceiver);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        CColliderParams param = new CColliderParams();
        param.owner = gameObject;
        param.other = collision.gameObject;
        SendMessageUpwards("OnChildrenTriggerStay2D", param, SendMessageOptions.RequireReceiver);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        CColliderParams param = new CColliderParams();
        param.owner = gameObject;
        param.other = collision.gameObject;
        SendMessageUpwards("OnChildrenTriggerExit2D", param, SendMessageOptions.RequireReceiver);
    }
}

public class CColliderParams
{
    public GameObject owner;
    public GameObject other;
}


