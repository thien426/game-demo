﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CDataManager
{
    static CDataManager _instance = null;
    public static CDataManager Instance { get { if (_instance == null) _instance = new CDataManager(); return _instance; } }

    CDataLevelManager _levels = null;
    public CDataLevelManager Levels { get { if (_levels == null) _levels = new CDataLevelManager(); return _levels; } }
}

#region LEVEL
public class CDataLevelManager
{
    int size;

    CDataLevel[] listDataLevel;

    public CDataLevelManager()
    {
        TextAsset[] listTXT = Resources.LoadAll<TextAsset>("Data/Levels");
        size = listTXT.Length;
        listDataLevel = new CDataLevel[size + 1];
        TextAsset itemTXT;
        for (int i = 0; i < size; i++)
        {
            itemTXT = listTXT[i];

            int index = int.Parse(itemTXT.name.Replace("Level_", ""));

            listDataLevel[index] = new CDataLevel();
            listDataLevel[index].Load(itemTXT);
        }
    }

    public CDataLevel GetLevelByID(int levelID)
    {
        return listDataLevel[levelID];
    }

    public CDataLevel GetRandomLevel()
    {
        return listDataLevel[UnityEngine.Random.Range(1, listDataLevel.Length)];
    }

    public int LevelLength()
    {
        return size;
    }
}

public class CDataLevel : CDataList
{
    public new CDataLevelItem GetItem(int index) { return base.GetItem(index) as CDataLevelItem; }
}

public class CDataLevelItem : CDataListItem
{
    public int indexRow;
    public List<int> listWallInRow;
    public List<int> listPositionWall;
    public int indexStarProcessInRow;
    public int countStarProcessInRow;
    public int indexStarBallInRow;
    public int countStarBallInRow;
    public List<int> listPositionBar;

    public override void Parse(string[] listStr)
    {
        indexRow = int.Parse(listStr[0]);

        listWallInRow = ListStringParse(listStr[1]);
        
        listPositionWall = ListStringParse(listStr[2], true);

        string intStr = ParseString(listStr[3]);
        string[] indexCountStr = intStr.Split('_');

        indexStarProcessInRow = int.Parse(indexCountStr[0]);
        countStarProcessInRow = int.Parse(indexCountStr[1]);

        intStr = ParseString(listStr[4]);
        indexCountStr = intStr.Split('_');

        indexStarBallInRow = int.Parse(indexCountStr[0]);
        countStarBallInRow = int.Parse(indexCountStr[1]);

        listPositionBar = new List<int>();

        intStr = ParseString(listStr[5]);
        indexCountStr = intStr.Split('_');

        for(int i = 0; i < indexCountStr.Length; i++)
        {
            int posBar = int.Parse(indexCountStr[i]);
            if (posBar > -1) listPositionBar.Add(posBar);
        }
    }

    string ParseString(string s)
    {
        return s.Replace("\"", "").Replace("\\n", "\n");
    }

    List<int> ListStringParse(string listStr, bool isPosition = false)
    {
        List<int> listInt = new List<int>();

        string intStr = ParseString(listStr);
        string[] listIntStr = intStr.Split('/');

        for (int i = 0; i < listIntStr.Length; i++)
        {
            string[] indexCount = listIntStr[i].Split('_');
            int index = int.Parse(indexCount[0]);
            int count = int.Parse(indexCount[1]);

            for (int j = 0; j < count; j++)
            {
                listInt.Add(index);

                if (isPosition) index++; // moi lan tang them 1
            }
        }

        return listInt;
    }
}
#endregion



/* Abstract */
public class CDataList
{
    protected CDataListItem[] listItems;

    public void Load(string fn)
    {
        List<string[]> listStr = ReadFile(fn);
        Process(listStr);
    }

    public void Load(TextAsset txt)
    {
        List<string[]> listStr = ReadTextAsset(txt);
        Process(listStr);
    }

    void Process(List<string[]> listStr)
    {
        int size = listStr.Count;
        //Debug.Log("CDataList => fn: " + fn + " -- size: " + size);
        string className = GetType().Name + "Item";
        listItems = new CDataListItem[size];
        for (int i = 1; i < size; i++)
        {
            listItems[i] = Activator.CreateInstance(System.Type.GetType(className)) as CDataListItem;
            listItems[i].Parse(listStr[i]);
        }
    }

    public CDataListItem[] GetListItems()
    {
        return listItems;
    }

    public CDataListItem GetItem(int index)
    {
        CDataListItem result = null;
        if (0 < index && index < listItems.Length)
        {
            result = listItems[index];
        }
        //Debug.Log("CDataItem => index: " + index + " -- result: " + result);
        return result;
    }


    public int GetTotalItems()
    {
        return GetListItems().Length - 1;
    }

    List<string[]> ReadFile(string fileName, int startLine = 0)
    {
        TextAsset asset = Resources.Load<TextAsset>(fileName);
        return ReadTextAsset(asset, startLine);
    }

    List<string[]> ReadTextAsset(TextAsset asset, int startLine = 0)
    {
        List<string[]> listLines = new List<string[]>();
        string[] lines = asset.text.Split('\n');
        string[] listItemInLine;
        for (int i = startLine; i < lines.Length; i++)
        {
            string line = lines[i].Trim();
            if (!string.IsNullOrWhiteSpace(line))
            {
                listItemInLine = line.Split('\t');
                listLines.Add(listItemInLine);
            }
        }
        return listLines;
    }
}

public class CDataListItem
{
    public virtual void Parse(string[] listStr) { }
}