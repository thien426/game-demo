﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CWallBase : MonoBehaviour
{
    public Collider2D collider;

    CStar star = null;

    public int indexWall;

    public Vector2 GetSize()
    {
        return collider.bounds.size;
    }

    public void HideWall()
    {
        Destroy(collider.gameObject);

        CGameStage.instance.totalWall--;

        if (star != null) star.SetRigid();

        if (CGameStage.instance.totalWall == 0) CPopupManager.instance.ShowResult();
    }

    public int IndexWall()
    {
        return indexWall;
    }

    public void InstanceStar(int indexStar)
    {
        string path = "Prefab/Stars/Start_" + indexStar;

        CStar prefab = Resources.Load<CStar>(path);

        star = Instantiate(prefab, transform);

        //star.gameObject.SetActive(false);
    }
}
