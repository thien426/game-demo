﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CPopupResult : MonoBehaviour
{
    [SerializeField] Text resultText;
    [SerializeField] Button nextButton;
    [SerializeField] Button againButton;

    public void Show()
    {
        gameObject.SetActive(true);

        Time.timeScale = 0;

        if (CGameStage.instance.totalWall == 0)
        {
            ShowNextButton();
        }
        else ShowAgainButton();
    }

    void ShowNextButton()
    {
        resultText.text = "YOU  WIN";

        nextButton.gameObject.SetActive(true);

        againButton.gameObject.SetActive(false);
    }

    void ShowAgainButton()
    {
        resultText.text = "YOU  LOSE";

        nextButton.gameObject.SetActive(false);

        againButton.gameObject.SetActive(true);
    }

    public void BackHander()
    {
        Time.timeScale = 1f;

        gameObject.SetActive(false);
    }

    public void NextButton()
    {
        CGameStage.instance.InstanceGame();

        BackHander();
    }

    public void AgainButton()
    {
        CUserInfo.Instance.level--;

        NextButton();
    }
}
