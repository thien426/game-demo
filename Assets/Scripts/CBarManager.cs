﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBarManager : MonoBehaviour
{
    public static CBarManager instance = null;
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    [SerializeField] GameObject barManager;

    float distanceWallRow = 1.4f;

    public void SetPositionBar(Vector2 posWallRow)
    {
        posWallRow.y -= distanceWallRow;

        barManager.transform.position = posWallRow;
    }
}
